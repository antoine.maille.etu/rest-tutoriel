package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InfoPrixDto extends InfoTauxDto {
	private double montantTotal;
	private double somme;
	private double montantTva;
	
	
	public InfoPrixDto() {
		
	}
	
	
	public InfoPrixDto(String label, double somme, double taux) {
		super(label, taux);
		this.somme = somme; 
		CalculTva calculTva = new CalculTva();
		this.montantTotal = calculTva.calculerMontant(TauxTva.valueOf(label), somme);
		this.montantTva = montantTotal - somme;
	}


	public double getMontantTotal() {
		return montantTotal;
	}
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getSomme() {
		return somme;
	}
	public void setSomme(double sommme) {
		this.somme = sommme;
	}
	public double getMontantTva() {
		return montantTva;
	}
	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}
	
	
	
	
}
